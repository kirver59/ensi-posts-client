# Ensi\BackendServiceSkeletonClient\VotesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createVotes**](VotesApi.md#createVotes) | **POST** /votes | Проголосовать
[**deleteVote**](VotesApi.md#deleteVote) | **DELETE** /votes/{id} | Удалить голос пользователя
[**getVote**](VotesApi.md#getVote) | **GET** /votes/{id} | Получение голоса пользователя
[**patchVote**](VotesApi.md#patchVote) | **PATCH** /votes/{id} | Изменить голос пользователя
[**searchOneVote**](VotesApi.md#searchOneVote) | **POST** /votes:search-one | Поиск голоса пользователя
[**searchVotes**](VotesApi.md#searchVotes) | **POST** /votes:search | Поиск голосов пользователей



## createVotes

> \Ensi\BackendServiceSkeletonClient\Dto\VoteResponse createVotes($create_vote_request)

Проголосовать

Проголосовать

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServiceSkeletonClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_vote_request = new \Ensi\BackendServiceSkeletonClient\Dto\CreateVoteRequest(); // \Ensi\BackendServiceSkeletonClient\Dto\CreateVoteRequest | 

try {
    $result = $apiInstance->createVotes($create_vote_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->createVotes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_vote_request** | [**\Ensi\BackendServiceSkeletonClient\Dto\CreateVoteRequest**](../Model/CreateVoteRequest.md)|  | [optional]

### Return type

[**\Ensi\BackendServiceSkeletonClient\Dto\VoteResponse**](../Model/VoteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteVote

> \Ensi\BackendServiceSkeletonClient\Dto\EmptyDataResponse deleteVote($id)

Удалить голос пользователя

Удалить голос пользователя

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServiceSkeletonClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteVote($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->deleteVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\BackendServiceSkeletonClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getVote

> \Ensi\BackendServiceSkeletonClient\Dto\VoteResponse getVote($id, $include)

Получение голоса пользователя

Получение голоса пользователя

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServiceSkeletonClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getVote($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->getVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\BackendServiceSkeletonClient\Dto\VoteResponse**](../Model/VoteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchVote

> \Ensi\BackendServiceSkeletonClient\Dto\VoteResponse patchVote($id, $patch_vote_request, $include)

Изменить голос пользователя

Изменить голос пользователя

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServiceSkeletonClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_vote_request = new \Ensi\BackendServiceSkeletonClient\Dto\PatchVoteRequest(); // \Ensi\BackendServiceSkeletonClient\Dto\PatchVoteRequest | 
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->patchVote($id, $patch_vote_request, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->patchVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_vote_request** | [**\Ensi\BackendServiceSkeletonClient\Dto\PatchVoteRequest**](../Model/PatchVoteRequest.md)|  |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\BackendServiceSkeletonClient\Dto\VoteResponse**](../Model/VoteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchOneVote

> \Ensi\BackendServiceSkeletonClient\Dto\SearchVotesResponse searchOneVote($search_votes_request)

Поиск голоса пользователя

Поиск голоса пользователя

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServiceSkeletonClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_votes_request = new \Ensi\BackendServiceSkeletonClient\Dto\SearchVotesRequest(); // \Ensi\BackendServiceSkeletonClient\Dto\SearchVotesRequest | 

try {
    $result = $apiInstance->searchOneVote($search_votes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->searchOneVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_votes_request** | [**\Ensi\BackendServiceSkeletonClient\Dto\SearchVotesRequest**](../Model/SearchVotesRequest.md)|  |

### Return type

[**\Ensi\BackendServiceSkeletonClient\Dto\SearchVotesResponse**](../Model/SearchVotesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchVotes

> \Ensi\BackendServiceSkeletonClient\Dto\SearchVotesResponse searchVotes($search_votes_request)

Поиск голосов пользователей

Поиск голосов пользователей

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServiceSkeletonClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_votes_request = new \Ensi\BackendServiceSkeletonClient\Dto\SearchVotesRequest(); // \Ensi\BackendServiceSkeletonClient\Dto\SearchVotesRequest | 

try {
    $result = $apiInstance->searchVotes($search_votes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->searchVotes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_votes_request** | [**\Ensi\BackendServiceSkeletonClient\Dto\SearchVotesRequest**](../Model/SearchVotesRequest.md)|  |

### Return type

[**\Ensi\BackendServiceSkeletonClient\Dto\SearchVotesResponse**](../Model/SearchVotesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

