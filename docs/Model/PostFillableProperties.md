# # PostFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **string** | Заголовок поста | [optional] 
**text** | **string** | Текст статьи | [optional] 
**active** | **bool** |  | [optional] [default to true]
**user_id** | **int** | Идентификатор пользователя | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


