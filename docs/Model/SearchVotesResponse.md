# # SearchVotesResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Ensi\BackendServiceSkeletonClient\Dto\Vote[]**](Vote.md) |  | [optional] 
**meta** | [**\Ensi\BackendServiceSkeletonClient\Dto\SearchPostsResponseMeta**](SearchPostsResponseMeta.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


