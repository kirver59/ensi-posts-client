# # Post

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор | [optional] 
**rating** | **int** | Рейтинг поста | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания поста | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления поста | [optional] 
**title** | **string** | Заголовок поста | [optional] 
**text** | **string** | Текст статьи | [optional] 
**active** | **bool** |  | [optional] [default to true]
**user_id** | **int** | Идентификатор пользователя | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


