<?php

namespace Ensi\BackendServiceSkeletonClient;

class BackendServiceSkeletonClientProvider
{
    /** @var string[] */
    public static $apis = [
        '\Ensi\BackendServiceSkeletonClient\Api\PostsApi',
        '\Ensi\BackendServiceSkeletonClient\Api\VotesApi',
    ];

    /** @var string[] */
    public static $dtos = [
        '\Ensi\BackendServiceSkeletonClient\Dto\CreatePostRequest',
        '\Ensi\BackendServiceSkeletonClient\Dto\CreateVoteRequest',
        '\Ensi\BackendServiceSkeletonClient\Dto\EmptyDataResponse',
        '\Ensi\BackendServiceSkeletonClient\Dto\Error',
        '\Ensi\BackendServiceSkeletonClient\Dto\ErrorResponse',
        '\Ensi\BackendServiceSkeletonClient\Dto\ModelInterface',
        '\Ensi\BackendServiceSkeletonClient\Dto\PaginationTypeCursorEnum',
        '\Ensi\BackendServiceSkeletonClient\Dto\PaginationTypeEnum',
        '\Ensi\BackendServiceSkeletonClient\Dto\PaginationTypeOffsetEnum',
        '\Ensi\BackendServiceSkeletonClient\Dto\PatchPostRequest',
        '\Ensi\BackendServiceSkeletonClient\Dto\PatchVoteRequest',
        '\Ensi\BackendServiceSkeletonClient\Dto\Post',
        '\Ensi\BackendServiceSkeletonClient\Dto\PostFillableProperties',
        '\Ensi\BackendServiceSkeletonClient\Dto\PostReadonlyProperties',
        '\Ensi\BackendServiceSkeletonClient\Dto\PostResponse',
        '\Ensi\BackendServiceSkeletonClient\Dto\RequestBodyCursorPagination',
        '\Ensi\BackendServiceSkeletonClient\Dto\RequestBodyOffsetPagination',
        '\Ensi\BackendServiceSkeletonClient\Dto\RequestBodyPagination',
        '\Ensi\BackendServiceSkeletonClient\Dto\ResponseBodyCursorPagination',
        '\Ensi\BackendServiceSkeletonClient\Dto\ResponseBodyOffsetPagination',
        '\Ensi\BackendServiceSkeletonClient\Dto\ResponseBodyPagination',
        '\Ensi\BackendServiceSkeletonClient\Dto\SearchPostsRequest',
        '\Ensi\BackendServiceSkeletonClient\Dto\SearchPostsResponse',
        '\Ensi\BackendServiceSkeletonClient\Dto\SearchPostsResponseMeta',
        '\Ensi\BackendServiceSkeletonClient\Dto\SearchVotesRequest',
        '\Ensi\BackendServiceSkeletonClient\Dto\SearchVotesResponse',
        '\Ensi\BackendServiceSkeletonClient\Dto\Vote',
        '\Ensi\BackendServiceSkeletonClient\Dto\VoteEnum',
        '\Ensi\BackendServiceSkeletonClient\Dto\VoteFillableProperties',
        '\Ensi\BackendServiceSkeletonClient\Dto\VoteReadonlyProperties',
        '\Ensi\BackendServiceSkeletonClient\Dto\VoteResponse',
        '\Ensi\BackendServiceSkeletonClient\Dto\VotesVoteEnum',
    ];

    /** @var string */
    public static $configuration = '\Ensi\BackendServiceSkeletonClient\Configuration';
}
